<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>ホームページのタイトル</title>
    <link rel="stylesheet" href="css/mypage.css">
  </head>
  <body>

      <h1>マイページ</h1>
      <section>
        <p>
          あなたのemailは info@webukatu.com です。<br />
          あなたのpassは password です。
        </p>
        <a href="index.php">ユーザー登録画面へ</a>
      </section>

  </body>
</html>
