<?php
$nums = array();

for($i=0; $i<5; $i++) {
  $col = range($i * 15 + 1, $i * 15 + 15);
  shuffle($col);
  $nums[$i] = array_slice($col, 0 , 5);
}

$nums[2][2] = "FREE";
// var_dump($nums);
// exit;

function h($s) {
  return htmlspecialchars($s, ENT_QUOTES, 'utf-8');
}

?>


<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>bingo!</title>
    <link rel="stylesheet" href="css/styles.css">
  </head>
  <body>
    <div id="container">
      <table>
        <tr>
          <th>B</th><th>I</th><th>N</th><th>G</th><th>O</th>
        </tr>
        <?php for($i=0; $i<5; $i++) : ?>
          <tr>
            <?php for($j=0; $j<5; $j++) : ?>
              <td><?php echo h($nums[$j][$i]); ?></td>
            <?php endfor; ?>
          </tr>
        <?php endfor; ?>
      </table>
    </div>
  </body>
</html>
