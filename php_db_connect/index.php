<?php

define('DB_DATABASE', 'dotinstall_db');
define('DB_USERNAME', 'dbuser');
define('DB_PASSWORD', 'Password');
define('PDO_DSN', 'mysql:dbhost=localhost;dbname=' . DB_DATABASE);
//DSN = Data Souce Name

class User {
  // public $id;
  // public $name;
  // public $score;
  public function show(){
    echo "$this->name ($this->score)";
  }

}

try {
  //connect
  $db = new PDO(PDO_DSN, DB_USERNAME, DB_PASSWORD);
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  //queryの抽出　select all
  // $stmt = $db->query("select * from users");

  //queryの条件付き抽出
  //score 60 以上
  // $stmt = $db->prepare("select score from users where score > ?");
  // $stmt->execute([60]);

  //name に f の文字列があるもの
  // $stmt = $db->prepare("select name from users where name like ?");
  // $stmt->execute(['%f%']);

  //scoreを逆順にして1位のレコードを抽出
  // $stmt = $db->prepare("select score from users order by score desc limit ?");
  //limit句を用いる場合、bindValueを使用する
  // $stmt->bindValue(1, 1, PDO::PARAM_INT);
  // $stmt->execute();

  // $users = $stmt->fetchAll(PDO::FETCH_ASSOC);
  // foreach ($users as $user) {
  //   var_dump($user);
  // }

  //FETCH_CLASS
  // $users = $stmt->fetchAll(PDO::FETCH_CLASS, 'User');
  // foreach ($users as $user) {
  //   $user->show();
  // }

  // update
  // $stmt = $db->prepare("update users set score = :score where name = :name");
  // $stmt->execute([
  //   ':score' => 100,
  //   ':name' => 'fsato'
  // ]);

  // echo "row updated:" . $stmt->rowCount();

  // delete
  // $stmt = $db->prepare("delete from users where name = :name");
  // $stmt->execute([
  //   ':name' => 'kasai'
  // ]);

  // echo "row deleted:" . $stmt->rowCount();

  //Transaction
  $db->beginTransaction();
  $db->exec("update users set score = score -10 where name = 'fsato'");
  $db->commit();


  // echo $stmt->rowCount() . " records found.";

  /*
  (1) exec(): 結果を返さない、安全なSQL
  (2) query(): 結果を返す、安全、何回も実行されないSQL
  (3) prepare(): 結果を返す、安全対策が必要、複数回実行されるSQL
  */

  //insert
  //(1) exec
  // $db->exec("insert into users (name, score) values ('kasai', 55)");
  // echo "user added!"

  //(3) prepare
  //stmt = statement object
  // $stmt = $db->prepare("insert into users (name, score) values (?, ?)");
  // $stmt->execute(['kasai', 44]);
  // echo "inserted: " . $db->lastInsertId();

  //(3') prepare name parameter
  // $stmt = $db->prepare("insert into users (name, score) values (:name, :score)");
  // $stmt->execute([':name'=>'fsato', ':score'=>'80']);
  // echo "inserted: " . $db->lastInsertId();

  // $stmt = $db->prepare("insert into users (name, score) values (?, ?)");
  // $stmt->execute(['kasai', 44]);
  // $stmt->execute(['kasai', 22]);
  // $stmt->execute(['kasai', 33]);
  //上記のような処理を行いたい場合、下記のように記述できる

  // $name = 'kasai';
  // $stmt->bindValue(1, $name, PDO::PARAM_STR);

  //bindValue: 値をbindする
  //name parameter使用時
  // $stmt->bindValue(':name', $name, PDO::PARAM_STR);

  // $score = 44;
  // $stmt->bindValue(2, $score, PDO::PARAM_INT);
  // $stmt->execute();
  // $score = 22;
  // $stmt->bindValue(2, $score, PDO::PARAM_INT);
  // $stmt->execute();
  // $score = 33;
  // $stmt->bindValue(2, $score, PDO::PARAM_INT);
  // $stmt->execute();

  //bindParam: 変数への参照をbind
  // $stmt->bindParam(2, $score, PDO::PARAM_INT);
  // $score = 12;
  // $stmt->execute();
  // $score = 23;
  // $stmt->execute();
  // $score = 34;
  // $stmt->execute();

  //PDO:PARAM_NULL
  //PDO:PARAM_BOOL

  //disconnect
  $db = null;

} catch (PDOException $e) {
  $db->rollback();
  echo $e->getMessage();
  exit;
}
 ?>
